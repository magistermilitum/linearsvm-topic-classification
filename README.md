### LinearSVM topic classification

* This is a Jupyter python notebook implementing LinearSVM (Support Vector Machine) pipeline to topic classification in latin and french
* Resulted model is trained on 20 thousand documents coming from 3 different sources
* Model is able to classify text in a set of 10 topics and 6 sub-categories. 

### Requeriments

* Anaconda 2.0 or higher
* sklearn Library
* Pandas
* Numpy